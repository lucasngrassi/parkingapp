package com.grupo5.parkingapp;

import android.content.Context;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.grupo5.parkingapp.Entities.Parking;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class HelperFirebase {

    private static FirebaseFirestore getInstance() { return FirebaseFirestore.getInstance(); }

    public static Query getParking(double latitude, double longitude, double distance) {
        // ~1 mile of lat and lon in degrees
        double lat = 0.0144927536231884;
        double lon = 0.0181818181818182;

        double lowerLat = latitude - (lat * distance);
        double lowerLon = longitude - (lon * distance);

        double greaterLat = latitude + (lat * distance);
        double greaterLon = longitude + (lon * distance);

        GeoPoint lesserGeopoint = new GeoPoint(lowerLat, lowerLon);
        GeoPoint greaterGeopoint = new GeoPoint(greaterLat, greaterLon);

        Query query = HelperFirebase.getInstance().collection("parkings")
                        .whereGreaterThan("geoposition", lesserGeopoint)
                        .whereLessThan("geoposition", greaterGeopoint)
                        .whereEqualTo("status", true);

        return query;
    }

    public static void saveParking(Parking parking) {
        final Parking parkingadd = parking;
        HelperFirebase.getInstance().collection("parkings").whereEqualTo("userid", parking.getUserid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        HelperFirebase.getInstance().collection("parkings").document(document.getId()).delete();
                    }
                }
                HelperFirebase.getInstance().collection("parkings").add(parkingadd);
            }
        });

    }

    public static Task<QuerySnapshot> getParkingUser(String userid){
        return HelperFirebase.getInstance().collection("parkings").whereEqualTo("userid", userid).get();
    }

    public static void removeParking(String id){

    }

    public static void updateParking(String id, boolean status){
        final boolean infoSet = status;
        HelperFirebase.getInstance().collection("parkings").whereEqualTo("userid", id).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        HelperFirebase.getInstance().collection("parkings").document(document.getId()).update("status", infoSet);
                    }
                }
            }
        });
    }

}
