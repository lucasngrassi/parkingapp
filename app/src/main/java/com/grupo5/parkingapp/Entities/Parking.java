package com.grupo5.parkingapp.Entities;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.GeoPoint;

public class Parking {
    private GeoPoint geoposition;
    private String address;
    private Timestamp createDate;
    private Boolean status;
    private String userid;

    public GeoPoint getGeoposition() {
        return geoposition;
    }

    public void setGeoposition(GeoPoint geoposition) {
        this.geoposition = geoposition;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
