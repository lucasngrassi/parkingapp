package com.grupo5.parkingapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

public class MapsActivity extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, LocationListener {

    private GoogleMap mMap;
    private LocationManager locationManager;
    private static final int MY_PERMISSIONS_REQUEST_ACCESSLOCATION = 999;
    private Location location = null;
    private LatLng ubicacion;
    private boolean mLocationPermissionGranted;
    private ListenerRegistration searchActive = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        View rootView = inflater.inflate(R.layout.activity_maps, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);  // use SuppoprtMapFragment for using in fragment instead of activity
        mapFragment.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Set a listener for marker click.
        mMap.setOnMarkerClickListener(this);
        preguntarPorPermisos();
    }

    /** Called when the user clicks a marker. */
    public boolean onMarkerClick(final Marker marker) {
        String URL = "https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&origin=" + ubicacion.latitude + "," + ubicacion.longitude + "&destination=" + marker.getPosition().latitude + "," + marker.getPosition().longitude;
        Uri location = Uri.parse(URL);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
        startActivity(mapIntent);

        // Return false to indicate that we have not consumed the event and that we wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return false;
    }

    private void preguntarPorPermisos() {
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            updateLocationUI();
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION },
                    MY_PERMISSIONS_REQUEST_ACCESSLOCATION);

        }
    }

    @Override
    public void onLocationChanged(Location location){
        ubicacion = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ubicacion));
        buscarParking(location);
    }

    private void buscarParking(Location location){
        if (this.searchActive != null) {
            this.searchActive.remove();
            this.mMap.clear();
        }

        this.searchActive = HelperFirebase.getParking(location.getLatitude(), location.getLongitude(), 1)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                mMap.clear();
                if (e != null) {
                    return;
                }

                if (snapshot != null) {
                    if (!snapshot.getDocuments().isEmpty()) {
                        for (DocumentSnapshot document : snapshot.getDocuments()) {
                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(document.getGeoPoint("geoposition").getLatitude(), document.getGeoPoint("geoposition").getLongitude()))
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                        }
                    }
                }
            }
        });

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESSLOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                iniciarGeolocalizacion();

            } else {
                mMap.setMyLocationEnabled(false);
                location = null;
                preguntarPorPermisos();
            }
        } catch (SecurityException e)  {
        }
    }

    private void iniciarGeolocalizacion() {
        try {
            mMap.setMyLocationEnabled(true);
            location = getLastKnownLocation();
            buscarParking(location);
            LatLng ubicacion = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ubicacion, 20));
        } catch (SecurityException e) {}
    }

    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        locationManager = (LocationManager)getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        String mejorProveedor = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission") Location loc = locationManager.getLastKnownLocation(provider);
            if (loc == null) {
                continue;
            }
            if (bestLocation == null || loc.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = loc;
                mejorProveedor = provider;
            }

        }
        if (mejorProveedor!=null){
            locationManager.requestLocationUpdates(mejorProveedor, 500, 1,  this);
        }

        return bestLocation;
    }

    @Override
    public void onResume(){
        super.onResume();
        updateLocationUI();
    }

    @Override
    public void onPause(){
        super.onPause();

        if (locationManager != null)
            locationManager.removeUpdates( this);
    }
}
