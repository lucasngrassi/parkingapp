package com.grupo5.parkingapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.auth0.android.Auth0;
import com.auth0.android.authentication.AuthenticationAPIClient;
import com.auth0.android.authentication.AuthenticationException;
import com.auth0.android.callback.BaseCallback;
import com.auth0.android.management.UsersAPIClient;
import com.auth0.android.result.UserProfile;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;
import com.grupo5.parkingapp.Entities.Parking;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

public class UserProviderActivity extends Fragment {

    private static UserProfile searchedProfile = null;
    private static Parking parking = null;
    private ProgressBar progressBar;
    private TextView lugarGuardadoPlaceholder;
    private Switch lugarGuardadoSwitch;
    private Auth0 auth0;
    private AuthenticationAPIClient authenticationAPIClient;
    private UsersAPIClient usersClient;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_user_provider, container, false);

        initAutocompleteWidget();

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        lugarGuardadoPlaceholder = getView().findViewById(R.id.lugar_placeholder_id);
        lugarGuardadoSwitch = getView().findViewById(R.id.lugar_guardado_switch_id);
        progressBar = getView().findViewById(R.id.activity_progress);

        lugarGuardadoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (parking != null) {
                    parking.setStatus(isChecked);

                    HelperFirebase.updateParking(searchedProfile.getId(), isChecked);
                }
            }
        });

        progressBar.setVisibility(View.VISIBLE);

        if (parking != null) {
            loadSavedPlace();
        }

        if (searchedProfile == null) {
            getProfile();
        }
    }

    private void getProfile() {
        auth0 = new Auth0(getActivity());
        auth0.setOIDCConformant(true);
        authenticationAPIClient = new AuthenticationAPIClient(auth0);
        usersClient = new UsersAPIClient(auth0, LoginActivity.EXTRA_ACCESS_TOKEN);

        authenticationAPIClient.userInfo(LoginActivity.EXTRA_ACCESS_TOKEN)
                .start(new BaseCallback<UserProfile, AuthenticationException>() {
                    @Override
                    public void onSuccess(UserProfile userinfo) {
                        /* Guardamos el usuario en memoria */
                        searchedProfile = userinfo;

                        /* Traemos los datos desde firebase */
                        loadSavedPlace();
                    }

                    @Override
                    public void onFailure(AuthenticationException error) {
                        /* Mostrar un mensaje de error */
                        Toast.makeText(getActivity(), "Ups no encontramos al perfil.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initAutocompleteWidget() {
        if (!Places.isInitialized()) {
            Places.initialize(getActivity().getApplicationContext(), getString(R.string.google_maps_key));
        }

        final AutocompleteSupportFragment autocompleteSupportFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_places_fragment);

        autocompleteSupportFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME));

        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                lugarGuardadoPlaceholder.setVisibility(View.VISIBLE);
                lugarGuardadoPlaceholder.setText(place.getName());
                lugarGuardadoSwitch.setVisibility(Switch.VISIBLE);
                lugarGuardadoSwitch.setChecked(true);

                parking.setGeoposition(new GeoPoint(place.getLatLng().latitude, place.getLatLng().longitude));
                parking.setAddress(place.getName());
                parking.setCreateDate(Timestamp.now());
                parking.setStatus(true);
                parking.setUserid(searchedProfile.getId());

                HelperFirebase.saveParking(parking);
            }

            @Override
            public void onError(@NonNull Status status) {
                /* Mostrar un mensaje de error */
                Toast.makeText(getActivity(), "Ups algo salio mal, vuelve a internarlo", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadSavedPlace() {
        if (parking != null) {
            lugarGuardadoPlaceholder.setVisibility(View.VISIBLE);
            lugarGuardadoPlaceholder.setText(parking.getAddress());
            lugarGuardadoSwitch.setVisibility(Switch.VISIBLE);
            lugarGuardadoSwitch.setChecked(parking.getStatus());

            progressBar.setVisibility(View.GONE);
        } else {
            parking = new Parking();
            HelperFirebase.getParkingUser(searchedProfile.getId())
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                            parking.setStatus(document.getBoolean("status"));
                            parking.setUserid(document.getString("userid"));
                            parking.setCreateDate(document.getTimestamp("createDate"));
                            parking.setAddress(document.getString("address"));
                            parking.setGeoposition(document.getGeoPoint("geoposition"));

                            loadSavedPlace();
                        }
                    }
                });

        }
    }
}
