package com.grupo5.parkingapp;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.auth0.android.authentication.AuthenticationException;
import com.auth0.android.authentication.storage.CredentialsManagerException;
import com.auth0.android.callback.BaseCallback;
import com.auth0.android.provider.AuthCallback;
import com.auth0.android.result.Credentials;

public class LoginActivity extends AppCompatActivity {

    private ProgressBar loaderSignIn;
    private Button loginButton;
    private TextView olvideContrasena;
    private ImageView googleSignIn;
    private String accessToken;
    private TextView credentialsView;
    private static final int CODE_DEVICE_AUTHENTICATION = 22;
    public static final String EXTRA_CLEAR_CREDENTIALS = "com.grupo5.CLEAR_CREDENTIALS";
    public static String EXTRA_ACCESS_TOKEN = "com.grupo5.ACCESS_TOKEN";
    public static String EXTRA_ID_TOKEN = "com.grupo5.ID_TOKEN";
    private AuthService authService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loaderSignIn = findViewById(R.id.loaderSignIn);
        loginButton = findViewById(R.id.loginButton);
        olvideContrasena = findViewById(R.id.olvideContrasena);
        googleSignIn = findViewById(R.id.googleSingIn);
        credentialsView = findViewById(R.id.credentials);

        /* Cuando hace click en el texto 'olvide contrasena' */
        olvideContrasena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loaderSignIn.setVisibility(v.VISIBLE);
                iniciarLogin();
            }
        });

        /* Cuando hace click en el icono de Google */
        googleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loaderSignIn.setVisibility(v.VISIBLE);
                iniciarLogin();
            }
        });

        /* Cuando hace click en el botton de logearse */
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loaderSignIn.setVisibility(v.VISIBLE);
                iniciarLogin();
            }
        });

        /* Obtain the token from the Intent's extras */
        accessToken = getIntent().getStringExtra(EXTRA_ACCESS_TOKEN);
        credentialsView.setText(accessToken);

        authService = AuthService.getInstance(this);

        if (authService.getCredentialsManager().hasValidCredentials()) {
            // Si tiene credenciales ya cargadas, entonces empezar la siguiente activity
            iniciarMainActivity();
        }
    }

    private void iniciarLogin() {
        authService.doLogin(this, loginCallback, getString(R.string.com_auth0_domain));
    }

    private final AuthCallback loginCallback = new AuthCallback() {
        @Override
        public void onFailure(@NonNull final Dialog dialog) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /* Esconder loader */
                    loaderSignIn.setVisibility(View.GONE);
                    dialog.show();
                }
            });
        }

        @Override
        public void onFailure(final AuthenticationException exception) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loaderSignIn.setVisibility(View.GONE);

                    /* Mostrar un mensaje de error */
                    Toast.makeText(LoginActivity.this, "Ups algo salio mal", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onSuccess(@NonNull Credentials credentials) {
            authService.setCredentialsManager(credentials);
            iniciarMainActivity();
        }
    };

    private void iniciarMainActivity() {
        authService.getCredentialsManager().getCredentials(new BaseCallback<Credentials, CredentialsManagerException>() {
            @Override
            public void onSuccess(final Credentials credentials) {
                Intent mainActivity = new Intent(LoginActivity.this, MainActivity.class);
                EXTRA_ACCESS_TOKEN = credentials.getAccessToken();
                EXTRA_ID_TOKEN = credentials.getIdToken();
                startActivity(mainActivity);
                finish();
            }

            @Override
            public void onFailure(CredentialsManagerException error) {
                // Autenticacion cancelado por el usuario. Cerramos la app
                Toast.makeText(LoginActivity.this, "Se require ingresar con una cuenta.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
