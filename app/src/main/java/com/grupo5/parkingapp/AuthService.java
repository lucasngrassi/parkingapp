package com.grupo5.parkingapp;

import android.app.Activity;
import android.content.Context;

import com.auth0.android.Auth0;
import com.auth0.android.authentication.AuthenticationAPIClient;
import com.auth0.android.authentication.storage.SecureCredentialsManager;
import com.auth0.android.authentication.storage.SharedPreferencesStorage;
import com.auth0.android.management.UsersAPIClient;
import com.auth0.android.provider.AuthCallback;
import com.auth0.android.provider.VoidCallback;
import com.auth0.android.provider.WebAuthProvider;
import com.auth0.android.result.Credentials;
import com.auth0.android.result.UserProfile;

// Singleton
public class AuthService {

    private static AuthService authService;
    private static Auth0 auth0;
    private static UsersAPIClient usersClient;
    private static SecureCredentialsManager credentialsManager;
    private static UserProfile searchedProfile;
    private static AuthenticationAPIClient authenticationAPIClient;

    public static AuthService getInstance(Context context) {
        if (authService == null) {
            authService = new AuthService();
            auth0 = new Auth0(context);
            auth0.setOIDCConformant(true);
            usersClient = new UsersAPIClient(auth0, LoginActivity.EXTRA_ACCESS_TOKEN);
            credentialsManager = new SecureCredentialsManager(context, authenticationAPIClient, new SharedPreferencesStorage(context));
        }

        return authService;
    }

    public SecureCredentialsManager getCredentialsManager() {
        return credentialsManager;
    }

    public void setCredentialsManager(Credentials credentials) {
        credentialsManager.saveCredentials(credentials);
    }


    public void doLogin(Activity activity, AuthCallback loginCallback, String domain) {
        WebAuthProvider.login(auth0)
                .withAudience(String.format("https://%s/userinfo", domain))
                .withScope("openid profile email offline_access read:current_user update:current_user_metadata")
                .start(activity, loginCallback);
    }


    public void doLogout(Context context, final VoidCallback logoutCallback) {
        credentialsManager.clearCredentials();
        logoutCallback.onSuccess(null);
    }

//    public static UserProfile getProfile(AuthenticationAPIClient authenticationAPIClient, Context context) {
////        auth0 = new Auth0(context);
////        auth0.setOIDCConformant(true);
////        authenticationAPIClient = new AuthenticationAPIClient(auth0);
////        usersClient = new UsersAPIClient(auth0, LoginActivity.EXTRA_ACCESS_TOKEN);
//
//        authenticationAPIClient.userInfo(LoginActivity.EXTRA_ACCESS_TOKEN)
//                .start(new BaseCallback<UserProfile, AuthenticationException>() {
//                    @Override
//                    public void onSuccess(UserProfile userinfo) {
//                        usersClient.getProfile(userinfo.getId())
//                                .start(new BaseCallback<UserProfile, ManagementException>() {
//                                    @Override
//                                    public void onSuccess(UserProfile profile) {
//                                        searchedProfile = profile;
//                                        Log.i("PROFILE", "=== PERFIL ID === " + profile.getId());
//
//                                    }
//
//                                    @Override
//                                    public void onFailure(ManagementException error) {
//                                        // Show error
//                                        Log.i("ERROROEROERKE", "second error " + error);
//                                    }
//                                });
//                    }
//
//                    @Override
//                    public void onFailure(AuthenticationException error) {
//                        // Show error
//                        Log.i("AuthService", "=== error === " + error);
//                    }
//                });
//
//        return searchedProfile;
//    }
}
