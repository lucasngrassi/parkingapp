package com.grupo5.parkingapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.auth0.android.Auth0Exception;
import com.auth0.android.provider.VoidCallback;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {

    AuthService authService;
    Fragment actualScreenSelected = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.nav_view);
        navigation.setOnNavigationItemSelectedListener(this);

        loadFragment(new MapsActivity());
        actualScreenSelected = new MapsActivity();

        authService = AuthService.getInstance(this);
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();

            return true;
        }

        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;

        switch ((menuItem.getItemId())) {
            case R.id.navigation_mapa:
                if (!(actualScreenSelected instanceof MapsActivity)) {
                    actualScreenSelected = new MapsActivity();
                    fragment = new MapsActivity();
                }
                break;
            case R.id.navigation_parking:
                if (!(actualScreenSelected instanceof UserProviderActivity)) {
                    actualScreenSelected = new UserProviderActivity();
                    fragment = new UserProviderActivity();
                }
                break;
            case R.id.navigation_logout:
                authService.doLogout(this, logoutCallback);
                break;
        }

        return loadFragment(fragment);
    }


    private VoidCallback logoutCallback = new VoidCallback() {
        @Override
        public void onSuccess(Void payload) {
            Intent mapsActivity = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(mapsActivity);
            finish();
        }

        @Override
        public void onFailure(Auth0Exception error) {
            // Log out canceled, keep the user logged in
            Toast.makeText(MainActivity.this, "Algo salio mal con el logout :(", Toast.LENGTH_SHORT).show();
        }
    };
}
